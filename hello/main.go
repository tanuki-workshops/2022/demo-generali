package main

import (
	"syscall/js"
)

func SayHello() string {
	return "hello"
}

func Handle(_ js.Value, args []js.Value) interface{} {

	jsonPayload := args[0]
	name := jsonPayload.Get("name").String()

	return map[string]interface{}{
		"message": "👋 Hello " + name + " 😃",
	}
}

func main() {
	println("🤖: hello wasm loaded")
	js.Global().Set("Handle", js.FuncOf(Handle))
	<-make(chan bool)
}
